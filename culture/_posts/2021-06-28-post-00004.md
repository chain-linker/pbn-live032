---
layout: post
title: "웹툰뷰어 ivViewer 시놀로지 설치 가이드"
toc: true
---

ivViewer는 쉽고 빠르게 웹툰을 감상할 이운 있는 php 기초 웹툰 뷰어입니다.
네이버, 다음, 한데 국소 웹툰의 메타데이터를 읽어 예 좋게 정리해줍니다.
 

 

 익금 글에서는 웹툰 뷰어 ivViewer를 시놀로지에 간단하게 설치할 핵 있는 방법에 대해서 서술합니다.
 ivViewer 주의사항
 + 저작권법을 준수하여야 합니다.
+ 뷰어로 정리된 웹툰은 일개인 소장용도로만 이용하여야 하고, 재배포, 판매, 공개 등의 행위를 하면 안됩니다.
+ 관련 소스를 악용하여 발생하는 상황에 대해 어떠한 책임도 조원 않습니다.
+ ivViewer는 앞서 다운 받은 웹툰을 메타데이터를 받아와 갈무리 및 뷰어 역할만 합니다. 다운로드는 지원하지 않
니다.
+ CC BY-NC-ND 2.0 KR 라이센스를 따릅니다. 수정 이다음 무단 식기 할 행복 없습니다.
 애드온을 추가로 판매중입니다.
 

 

 시놀로지의 본보기 자동 업데이트는 지원하지 않습니다!

## 1. 시놀로지 웹스테이션 활성화.
 ivViewer는 웹기반으로 작동하는 웹툰 뷰어입니다. 미리감치 웹스테이션을 활성화 해주어야 합니다.
 시놀로지 버전에 따라 활성화 방법이 다르니, 밑 방법을 참고해주세요.

### 방편 1

 시놀로지 제어판
 우선, 시놀로지에 관리자 권한으로 로그인한 뒤, 제어판 -> 웹서비스로 이동합니다.

 제어판 -> 웹 서비스
 갑 상단에 있는 Web Station 활성화를 점검 경계 뒤, 선용 버튼을 클릭합니다.

### 방법2

 패키지센터 웹스테이션
 시놀로지의 패키지 센터 -> 유틸리티 -> Web Station 를 설치해주세요.

## 2. ivViewer, 웹스테이션 장치 및 설정
 우선, https://blog.ivlis.kr/post/105 에서 다운로드(ZIP) 버튼을 클릭하여 파일을 다운로드 합니다.
ZIP 파일의 압축을 해제하면, ivViewer-master 폴더가 생성됩니다.

 시놀로지 /web 폴더
 

 

 압축을 해제한 ivViewer-master 폴더를 시놀로지 /web 폴더에 업로드 해주세요. (가이드에서는 폴더명의 -master 를 삭제 차기 진행했습니다.)
 

 웹스테이션
 

 재처 아이콘 클릭 시, 패키지 센터로 이동됨.
 설치한 웹스테이션을 실행하여, Nginx 또 Apache HTTP Server 2.4 (★권장) 와 PHP 7.3 버전 (2020-04-23 작량 최신버전)의 조처 버튼을 클릭하여 패키지 센터에서 설치합니다.

 PHP 설정
 

 

 웹스테이션의 PHP 설정으로 이동하여, 설치한 PHP 프로파일을 체크 후, 편집을 눌러줍니다.

 PHP 고급 설경 창
 

 

 확장 플러그인을 모두 점검 후, 확인 버튼을 눌러줍니다.

 가상 호스트 생성
 

 

 웹스테이션 -> 가설 호스트 -> 생성에서 새로운 가정 호스트를 만들어줍니다.
 도메인을 연결하고 싶다면 호칭 기반을, 시놀로지 접속 주소에 포트만 바꿔 사용하고 싶다면 포트 기반을 선택합니다.
 HTTP 백엔드 서버와 PHP를 자신이 설치한 백엔드와 자신이 설정한 프로파일로 주의 후 확인을 눌러줍니다.

 집단 영향력 설정
 

 일당 힘 설정
 

 

 시놀로지 제어판 -> 그룹 -> http -> 편집 -> 실력 web 폴더 및 연결할 만화가 있는 폴더의 읽기/쓰기 권한을 검열 다음 확인 버튼을 눌러주세요.
 (*하단은 초석 폴더를 생성하는 필수과정 입니다 건너뛰지 마세요.)
 

 

 로그인 페이지
 

기본 계정 아이디 : ivuser
기본 계정 비밀번호: ivpass

설정한 주소/포트로 ivViewer 에 접속 버금 로그인 합니다.

 메인화면
 

 

 정상적으로 메인 화면으로 접속 되셨다면 정상적으로 성품 폴더가 생성 되었을 것 입니다. 기저 계정정보 변경 버튼을 클릭하여 아이디와 비밀번호를 동부동 변경해주세요.
 * 여기서 메인 화면으로 이동이 되지 않을 경우, 자력 문제일 가능성이 큽니다. 상단 과정을 잼처 진행하세요.

## 

## 3. 폴더 견련 스케줄러 등록 및 실행
 뷰어가 장치 되었다면, 웹툰 폴더를 연결해 줘야합니다.

 작업 스케줄러
 

 제어판 -> 역사 스케줄러에서 생성-> 트리거된 노작 -> 사용자 의향 스크립트를 클릭합니다.

 노무 생성
 

 그럭저럭 작업 이름을 설정합니다. (상관 없습니다. 보기 편하게 설정하시면 됩니다.)
 활성화됨 검토 뒤 작업 설정으로 이동합니다.

 천행 명령 설정
 

 사용자 의향 스크립트에 밑 내용을 참고하여 입력합니다.
 ln -s "/volume1/data/naverwebtoon" "/volume1/web/ivViewer/data/"
 해석하자면, ln -s "A" "B" 명령어는 A 폴더를 B 폴더 하위에 마운트 하라는 의향 입니다.
 

 A 에는 "/volume[볼륨[웹툰 추천](https://imgcompression.com/culture/post-00000.html)숫자]/경로/웹툰폴더"를, B에는 "/volume[볼륨숫자]/web/ivViewer(아까 업로드한 폴더명)/data/" 를 입력하면 됩니다. (! A의 노하우 마지막에는 / 결미 붙지 않습니다.)
 웹툰폴더의 하위 폴더 구조는 아래와 같습니다.
 

 자질 폴더 구조: /volume[볼륨숫자]/경로/웹툰폴더/웹툰이름/001 웹툰이름 01화 제목.zip/.png
 Ex: /volume1/download/manhwa/네이버[완결]/가타부타타/001 가타부타타 01화 제목.zip
Ex: /volume1/download/manhwa/다음[연재]/이태원 클라쓰/001 이태원 클라쓰 01화 제목.png
 

 (! /웹툰제목/회차이름/001.png 형식의 폴더로 된 회차는 지원하지 않습니다.)
 

 * 볼륨의 숫자는 경로 만화가 저장된 폴더와 web 폴더가 다를 행우 있습니다. 본인이 설정한 의려 폴더 속성을 확인하세요.

 

 역사 스케줄러 창에서 고대 생성한 사용자 뜻 스크립트를 클릭 후, 이천 버튼을 클릭합니다.
 

## 4. 메타데이터 생성 (선택 사항)
 ivViewer 로 접속 이후 로그인해줍니다.

 접속 메인페이지
 웹툰이 정상적으로 표시된다면, 즉 작동하고 있는 것 입니다.

 페이지 하단
 웹툰 뷰어의 페이지 하단으로 내리면, 메타데이터 등록 버튼이 있습니다. 해당 버튼을 클릭하세요.

 메타데이터가 생성중인 모습
 급기야 잠깐 후, 메타데이터 등록을시작합니다. 만화 하나에 서버 사양에 따라 3초 ~ 5초 스케일 소요됩니다. 인내심을 가지고 기다려주세요.

 메타데이터 등록 완료
 메타데이터가 전면 생성되면, 위와 같은 메세지가 뜹니다.

 메타데이터가 서두 생성된 모습
 메타데이터가 실총 생성되면 위와 같은 모습으로 사용할 요행 있습니다.

## 5. 최적화 (설정 페이지)
 메타데이터 솔성 로드: (기본값 : 비활성화)
 생성된 메타데이터 구조를 바탕으로 메인 화면을 불러옵니다. 메타데이터가 생성되지 않은 웹툰은 표시하지 않습니다. Rclone 등, 타 폴더를 마운트 어째서 사용할 경우에 활성화 하는 것을 권장합니다.
 이미지 로드: (기본값 : 활성화)
 메인화면에 불러오는 웹툰 목록에 섬네일을 표시합니다.
 

## 끝내며..
 ivViewer는 2020년 조준 고등학생인 형제무루 단신 개발하고 배포하는 프로젝트로, 의도치 않은 버그가 발생할 생명 있는 점, 이내 수정할 운명 없는 점을 상천 숙지해 주시길 바랍니다.
 이목 있으시다면 위에 언급한 에드온도 한번 살펴보세요.
 으레 좋은 프로그램을 만들 효능 위해, 후원을 받고 있습니다! 바로 사용하고 있다면, 작은 금액이라도 후원해주세요!
 tvj030728@ivlis.kr | 각항 내용 환영합니다!

#### '개발 > 사용법' 카테고리의 다른 글
